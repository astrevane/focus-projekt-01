Instrukcja:

- aby pobrać projekt z repozytorium, w okne konsoli wprowadzamy komendę:
    git clone https://astrevane@bitbucket.org/astrevane/focus-projekt-01.git

- przechodzimy w konsoli do folderu focus-projekt-01 komendą:
    cd focus-projekt-01

- pobieramy niezbędne biblioteki komendą:
    npm install

- uruchamiamy aplikację komendą:
    node dialer.js
