const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors')
const bodyParser = require('body-parser')
const http = require('http').Server(app)
const io = require('socket.io')(http)


const closeStatus = ["ANSWERED", "FAILED", "BUSY", "NO ANSWER"]
let id = 0
let callHistory = []

const config = {
    url: 'https://uni-call.fcc-online.pl',
    login: 'focus07',
    password: 'gdf56fofbw'
};


Dialer.configure(config);
app.use(cors())
app.use(bodyParser.json())

http.listen(3000, () => {
    console.log('app listening on port 3000');
});

io.on('connection', (socket) => {
    console.log('a user connected')
    socket.on('disconnect', () => {
        console.log('a user disconnected')
    })
    socket.on('message', (message) => {
        console.log('message', message)
    })
    io.emit('message', 'connected!')
})


app.get('/status/:id', async (req, res) => {
    let status = callHistory[req.params.id]
    if (!status) {
        res.json({ success: false, status: "NO SUCH ID" });
        return
    }
    res.json({ success: true, status: status });
});



app.post('/call/', async (req, res) => {
    const body = req.body    
    let status = null
    const currId = id
    ++id
    let bridge
    try {
        bridge = await Dialer.call(body.number1, body.number2)
    } catch (e) {
        res.json({ success: false })
        return
    }
    let interval = setInterval(async () => {
        let oldStatus = status
        status = await bridge.getStatus();
        if (status != oldStatus) {
            callHistory[currId] = status
            io.emit('status', 'connection id ' + currId + ': ' + status)
        }
        if (closeStatus.includes(status)) {
            console.log("clearing interval for id:", currId)
            clearInterval(interval)
        }
    }, 500)   
    res.json({ id: currId, success: true })
})